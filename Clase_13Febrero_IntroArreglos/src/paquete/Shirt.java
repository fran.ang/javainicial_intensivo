package paquete;

public class Shirt {
    
    private int shirtID;
    private float price;
    private char colorCode;

    public Shirt(int shirtID, float price, char colorCode) {
        this.shirtID = shirtID;
        this.price = price;
        this.colorCode = colorCode;
    }

    public Shirt() {
    }

    public int getShirtID() {
        return shirtID;
    }

    public void setShirtID(int shirtID) {
        this.shirtID = shirtID;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public char getColorCode() {
        return colorCode;
    }

    public void setColorCode(char colorCode) {
        this.colorCode = colorCode;
    }

    @Override
    public String toString() {
        return "Shirt{" + "shirtID=" + shirtID + ", price=" + price + ", colorCode=" + colorCode + '}';
    }       
    
}
