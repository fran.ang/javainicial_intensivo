
package paquete;

public class Principal {

    public static void main(String[] args) {
        
        //Declaracion e instanciacion
        int[] edades = new int[4];
        /*edades[0] = 19;
        edades[1] = 42;
        edades[2] = 99;
        edades[3] = 33;*/
        for (int i = 0; i < edades.length; i++) {
            System.out.println("Cargar edades en i = " + i);
            edades[i] = Consola.readInt();
        }
        
        //Otra forma:
        //int[] edades = {19, 42, 99, 33};
        
        /*
        System.out.println("edades [" + 0 + "]: " + edades[0]);
        System.out.println("edades [" + 1 + "]: " + edades[1]);
        System.out.println("edades [" + 2 + "]: " + edades[2]);
        System.out.println("edades [" + 3 + "]: " + edades[3]);
        */
        
        System.out.println("Tamaño del arreglo edades: " + edades.length);
        for (int i = 0; i < edades.length; i++) {
            System.out.println("edades [" + i + "]: " + edades[i]);
        }
        
        //for loop
        for (int edad : edades) {
            System.out.println("edad: " + edad);
        }
        
        
        char[] letras = {'a', 'b', 'c'};
        for (int i = 0; i < letras.length; i++) {
            System.out.println("letras [" + i + "]: " + letras[i]);
        }
        
    }
    
}
