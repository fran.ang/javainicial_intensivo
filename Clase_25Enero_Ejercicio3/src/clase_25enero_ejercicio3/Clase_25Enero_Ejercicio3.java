
package clase_25enero_ejercicio3;

public class Clase_25Enero_Ejercicio3 {   
    
    //VARIABLES GLOBALES - O CAMPOS
    public static int nro1, mayor, menor, nro2, supCuad;
    public static float supCirc;

    public static void main(String[] args) {
                
        System.out.println("Ingrese un numero: ");
        nro1 = Consola.readInt();
        
        System.out.println("Ingrese otro numero: ");
        nro2 = Consola.readInt();        
        
        //Aca se ordena
        ordenar();
        
        //Aca se calcula
        calcular();        
               
    }//Termina main()
    
    public static void ordenar(){          
        if(nro1 > nro2){
            mayor = nro1;
            menor = nro2;
        }else{
            mayor = nro2;
            menor = nro1;
        }
        
        System.out.println("El mayor: " + mayor);
        System.out.println("El menor: " + menor);
        
    }//Termina ordenar()
    
    public static void calcular(){
        supCuad = mayor * mayor;
        supCirc = 3.14f * menor * menor;
        
        System.out.println("La sup. del cuadrado es: " + supCuad + " cm^2");
        System.out.println("La sup. del circulo es: " + supCirc + " cm^2");        
    }//Termina calcular()
        
}//Termina clase
