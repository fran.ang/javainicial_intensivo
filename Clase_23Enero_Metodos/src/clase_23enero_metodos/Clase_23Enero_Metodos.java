package clase_23enero_metodos;

public class Clase_23Enero_Metodos {
    
    //VARIABLES GLOBALES
    // modificador [static] tipo nombre;
    public static int nro1, nro2, nro3, men; //Declarar una variable   
    
   
    public static void obtenerMenor(){
        if(nro1 < nro2 && nro1 < nro3){
            men = nro1;
        }else if(nro2 < nro3){
            men = nro2;
        }else{
            men = nro3;
        }
        
        System.out.println("El menor es: " + men);       
    } //Termina obtenerMenor()
    
    public static void calcular(){
        int cuad, cubo;
        cuad = men * men;
        cubo = cuad * men;
        
        System.out.println("El cuadrado de " + men + " es: " + cuad);
        System.out.println("El cubo de " + men + " es: " + cubo);    
        System.out.println("das");
    } //Termina calcular()
    
    public static void main(String[] args) {
        
        //VARIABLES LOCALES (al metodo main)
        // tipo nombre;
        
        nro1 = 5; //Inicializar una variable
        nro2 = 10;
        nro3 = 4;        
        
        //PROCESO - SUB1
        obtenerMenor();
          
        //PROCESO - SUB2
        calcular();
        
    } //Termina main
    
} //Termina la clase
