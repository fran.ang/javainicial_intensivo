package clase_30enero_metodos;

public class Clase_30Enero_Metodos {
    
    //VARIABLES GLOBALES
    // modificador [static] tipo nombre;
   
    public static int obtenerMenor(int numero1, int numero2, int numero3){
        int men = 0;
        
        if(numero1 < numero2 && numero1 < numero3){
            men = numero1;
        }else if(numero2 < numero3){
            men = numero2;
        }else{
            men = numero3;
        }
        
        return men;      
    } //Termina obtenerMenor()
    
    public static int calcularCuadrado(int men){
        int cuad;        
        cuad = men * men;               
        return cuad;
    } //Termina calcular()
    
    public static int calcularCubo(int men){
        int cubo;
        cubo = calcularCuadrado(men) * men;        
        return cubo;
    }
    
    public static void main(String[] args) {
        
        //VARIABLES LOCALES (al metodo main)
        // tipo nombre;
        
        int nro1 = 5; //Inicializar una variable
        int nro2 = 10;
        int nro3 = 4;        
        
        //PROCESO - SUB1
        int menor = obtenerMenor(nro1, nro2, nro3); //Declarando e inicializando
        System.out.println(menor);
        System.out.println("El menor es: " + menor);
          
        //PROCESO - SUB2
        int cuadrado = calcularCuadrado(menor);
        System.out.println("El cuadrado del menor es: " + cuadrado);

        int cubo = calcularCubo(menor);
        System.out.println("El cubo del menor es: " + cubo);        
        
    } //Termina main
    
} //Termina la clase
