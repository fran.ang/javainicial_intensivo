package paquete;

public class Principal {

    private static Arreglo objArreglo;  //Solo esta declarado
    
    public static void main(String[] args) {
        
        System.out.print("Ingrese la cantidad de elementos del arreglo: ");
        int tamanio = Consola.readInt();
        
        //Instanciamos el arreglo:
        objArreglo = new Arreglo(tamanio);
        
        System.out.println("El tamanio del arreglo es: " + objArreglo.length());
        
        System.out.println("Arreglo sin cargar:" + objArreglo); //Antes era mostrar()      
        
        //Cargamos el arreglo que contiene el objeto
        cargar();
        System.out.println("\nArreglo con carga: " + objArreglo); //Antes era mostrar()        
        
        System.out.println("\nIngresar un valor K para multiplicar: ");
        int k = Consola.readInt();       
        objArreglo.multiplicar(k); //Antes era multiplicar(k);
        System.out.println("\nArreglo multiplicado por K: " + objArreglo); //Antes era mostrar();               
        
    } //Fin main()
    
    public static void cargar(){
        //En cada vuelta del for, solicitar un valor cargado por teclado
        for(int i = 0; i < objArreglo.length(); i++){
            System.out.print("Ingrese el elemento " + i +" para el arreglo: ");
            int valor = Consola.readInt();
            objArreglo.setComponent(valor, i);
        }
    }//Fin cargar()
 
    
}//Fin de clase
