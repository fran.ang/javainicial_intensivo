package paquete;


public class Arreglo {
    
    //ATRIBUTO GLOBAL O DE LA CLASE
    private int[] arreglo;
    
    
    //CONSTRUCTORES
    public Arreglo(){
        arreglo = new int[5];
    }
    
    public Arreglo(int tamanio){        
        arreglo = new int[tamanio];
    }
    
    //METODOS
    public int getComponent(int i){
        return arreglo[i];
    }
    
    public void setComponent(int valor, int i){
        arreglo[i] = valor;
    }
    
    public int length(){
        return arreglo.length;
    }
    
    public void multiplicar(int k){
        //Recorre el arreglo y lo multiplica por un valor K
        for(int i = 0; i < arreglo.length; i++){
            arreglo[i] *= k; 
        }
        //variable = variable operador expresion;
        //Reducido: variable operador= expresion;
       
    }
    
    public String toString(){
        String cadena = "";
        
        // Recorrer el arreglo y guardar sus elementos en una variable String         
        for(int i = 0; i < arreglo.length; i++){            
            cadena += arreglo[i] + " ";
        }
        
        return cadena;
    }
    
    
}
