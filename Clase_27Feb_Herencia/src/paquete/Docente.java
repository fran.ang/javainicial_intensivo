package paquete;

public class Docente extends Persona{
    
    private String profesion;

    public Docente(int dni, String nombre, String apellido, String profesion) {
        super(dni, nombre, apellido);
        this.profesion = profesion;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }
    
    @Override
    public String toString() {
        return super.toString() + "Docente{" + "profesion=" + profesion + '}';
    }    

    @Override
    public void metodoAbs() {
        System.out.println("Soy abstracto en Docente");
    }
    
}
