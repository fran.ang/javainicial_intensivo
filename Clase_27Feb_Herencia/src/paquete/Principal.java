package paquete;

public class Principal {

    public static void main(String[] args) {
        
        Persona per;
    
        
        Persona est = new Estudiante(38000000, "Marcos", "Perez", 9.8);        
        mostrarInfo(est);
        
        Persona doc = new Docente(39000000, "Juan", "Lopez", "Matematico");
        mostrarInfo(doc);
        
        Persona[] personas = new Persona[5];
        personas[0] = new Estudiante(111,"aa","bb",9.9);
        personas[1] = new Docente(222,"cc","dd","Ingeniero");
        personas[2] = new Estudiante(333,"dd","ll",10);
       
        for (int i = 0; i < personas.length; i++) {
            System.out.println(personas[i]);
        }
    }
    
    public static void mostrarInfo(Persona per){
        System.out.println(per);
        
        if (per instanceof Docente) {
            Docente doc = (Docente) per;
            System.out.println("La profesion es: " + doc.getProfesion());
        }else{
            Estudiante est = (Estudiante) per;
            est.mostrarUnMensaje();
            System.out.println("Promedio: " + est.getPromNotas());
        }
    }
    
    
    
    
}
