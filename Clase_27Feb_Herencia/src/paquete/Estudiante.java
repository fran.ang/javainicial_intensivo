package paquete;

public class Estudiante extends Persona implements Mensaje{
    
    //ATRIBUTOS: id, nombre, apellido
    private double promNotas;

    public Estudiante() {
        //super();
    }        
    
    public Estudiante(int dni, String nombre, String ape, double promNotas){
        super(dni, nombre, ape);
        this.promNotas = promNotas;       
    }

    public double getPromNotas() {
        return promNotas;
    }

    public void setPromNotas(double promNotas) {
        this.promNotas = promNotas;
    }

    @Override
    public String toString() {
        return super.toString() + "Estudiante{" + "promNotas=" + promNotas + '}';
    }

    @Override
    public void metodoAbs() {
        System.out.println("Soy abstracto con Estudiante");
    }

    @Override
    public void mostrarUnMensaje() {
        System.out.println("Soy un mensaje");
    }
    
    
    
    
}
