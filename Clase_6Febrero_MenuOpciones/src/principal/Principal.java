package principal;

public class Principal {

    public static void main(String[] args) {

        System.out.println("Ingrese un numero: ");
        int nro1 = Consola.readInt();

        System.out.println("Ingrese otro numero: ");
        int nro2 = Consola.readInt();

        Operacion operaciones = new Operacion(nro1, nro2);
        int opcion;

        do {
            System.out.println("=== MENU DE OPCIONES ===");
            System.out.println("1 - Sumar");
            System.out.println("2 - Restar");
            System.out.println("3 - Salir");
            System.out.println("Ingresar una opcion: ");
            opcion = Consola.readInt();

            switch (opcion) {

                case 1:
                    int suma = operaciones.sumar();
                    System.out.println("La suma es: " + suma);
                    break;
                case 2:
                    int resta = operaciones.restar();
                    System.out.println("La resta es: " + resta);
                    break;
                case 3:
                    System.out.println("Adios!");
                    break;
                default:
                    System.out.println("No es una opcion valida");
            }
        } while (opcion != 3);

    }

}
