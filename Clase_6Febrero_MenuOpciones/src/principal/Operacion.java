
package principal;

public class Operacion {
    
    //ATR. GLOBALES
    private int nro1;
    private int nro2;
    
    //CONSTRUCTOR
    public Operacion() {
    }

    public Operacion(int nro1, int nro2) {
        this.nro1 = nro1;
        this.nro2 = nro2;
    }
    
    //METODOS

    public int getNro1() {
        return nro1;
    }

    public void setNro1(int nro1) {
        this.nro1 = nro1;
    }

    public int getNro2() {
        return nro2;
    }

    public void setNro2(int nro2) {
        this.nro2 = nro2;
    }
    
    public int sumar(){
        return nro1 + nro2;
    }
    
    public int restar(){
        return nro1 - nro2;
    }
    
    public int multiplicar(){
        return 0;
    }
    
    public int dividir(){
        return 0;
    }
    
    @Override
    public String toString() {
        return "Operacion{" + "nro1=" + nro1 + ", nro2=" + nro2 + '}';
    }                
}
