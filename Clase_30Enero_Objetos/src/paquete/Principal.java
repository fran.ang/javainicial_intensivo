
package paquete;

public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Primer objeto en java
        Perro miPerro = new Perro();
        
        miPerro.raza = "Bulldog";
        miPerro.color = "Blanco";
        miPerro.peso = 3.5;
        miPerro.edad = 7;
        
        System.out.println("Mi objeto perro: " + miPerro.toString());
        
        //Segundo objeto java
        Perro labrador = new Perro();
        labrador.raza = "Labrador";
        labrador.color = "Amarillo";
        labrador.peso = 8.5;
        labrador.edad = 5;
        
        System.out.println("Mi labrador: " + labrador.toString());
        
        labrador.dormir();
    }
    
}
