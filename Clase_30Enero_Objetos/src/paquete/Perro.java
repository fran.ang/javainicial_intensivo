
package paquete;

/**
 *
 * @author franc
 */
public class Perro {
    
    //ATRIBUTOS DE LA CLASE - GLOBALES
    //+ public, - private
    //modificador tipo nombre;
    public String raza;
    public String color;
    public double peso;
    public int edad;
    
    //METODOS
    public String ladrar(){
        return "El perro esta ladrando";
    }
    
    public void dormir(){
        System.out.println("El perro esta durmiendo");
    }
    
    public String comer(){
        return "El perro esta comiendo";
    }
    
    public String ladrarYComer(){
        return "El perro esta ladrando y comiendo";
    }
    
    public String toString(){
        return "Raza: " + raza + " Color: " + color + " Peso: " + peso + " Edad: " + edad;
    }
    
}
