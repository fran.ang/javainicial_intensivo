package paquete;

public class Principal {
    
    private static Tienda tienda;

    public static void main(String[] args) {
        int opcion;
        //MENU DE OPCIONES
        do{
            System.out.println("== MENU ==");
            System.out.println("1 - Iniciar / Reiniciar articulos");
            System.out.println("2 - Cargar articulos");
            System.out.println("3 - Buscar un articulo por ID");
            System.out.println("4 - Mostrar solo articulos vendidos");
            System.out.println("5 - Mostrar todos los articulos");
            System.out.println("6 - Salir");
            System.out.println("Ingrese una opcion: ");
            opcion = Consola.readInt();
            
            switch(opcion){
                case 1: 
                    System.out.println("Ingrese la cantidad de articulos: ");
                    int cant = Consola.readInt();
                    tienda = new Tienda(cant);
                    break;
                case 2: 
                    cargar();
                    break;
                case 3:
                    System.out.println("Ingrese un ID de un articulo: ");
                    int id = Consola.readInt();
                    buscarPorId(id);
                    break;
                case 4:
                    break;
                case 5:
                    System.out.println(tienda);
                    break;
                case 6:
                    System.out.println("Hasta luego!");
                    break;
                default:
                    System.out.println("La opcion no existe, por favor ingrese otra.");
            }
            
        }while( opcion != 6);
        
    }
    
    public static void cargar(){
        //La logica para solicitar los datos de un articulo y cargarlo en el
        //arreglo de tienda
        for (int i = 0; i < tienda.getCantidadArticulos(); i++) {
            System.out.println("CARGA DEL ARTICULO NRO. " + i);
            System.out.println("Ingrese el ID: ");
            int id = Consola.readInt();
            
            System.out.println("Ingrese el nombre: ");
            String nombre = Consola.readLine();
            
            System.out.println("Esta vendido? (S , N)");
            char letra = Consola.readChar();
            boolean estaVendido = (letra == 'S');
            
            Articulo art = new Articulo(id, nombre, estaVendido);
            
            tienda.setArticulo(art, i);            
        }
        
    }
    
    public static void buscarPorId(int id){
        //Aca solamente llamamos al buscar() de Tienda 
        //y mostramos su resultado
        String nombreBusq = tienda.buscar(id);
        System.out.println("El articulo buscado es: " + nombreBusq);
    }
    
    public static void mostrarVendidos(){
        
    }
    
}
