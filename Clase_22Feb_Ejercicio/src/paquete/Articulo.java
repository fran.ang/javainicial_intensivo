package paquete;

public class Articulo {
    
    //ATRIBUTOS GLOBALES O DE LA CLASE
    private int id;
    private String nombre;
    private boolean estaVendido;
    
    
    //CONSTRUCTORES
    public Articulo() {
        id = -1;
        nombre = "SIN_NOMBRE";        
    }

    public Articulo(int id) {
        this.id = id;
        nombre = "SIN_NOMBRE";
    }

    public Articulo(int id, String nombre, boolean estaVendido) {
        this.id = id;
        this.nombre = nombre;
        this.estaVendido = estaVendido;
    }
    
    //METODOS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getEstaVendido() {
        return estaVendido;
    }

    public void setEstaVendido(boolean estaVendido) {
        this.estaVendido = estaVendido;
    }

    @Override
    public String toString() {
        return "ID: " + id + 
                ", nombre: " + nombre + 
                ", ¿Esta vendido?: " + estaVendido;
    }         
    
}
