package paquete;

public class Tienda {
    
    //ATRIBUTOS GLOBLAES O DE LA CLASE
    private String nombre;
    private Articulo[] articulos;
    
    //CONSTRUCTOR
    public Tienda(){
        nombre = "Informatica Cordoba";
        articulos = new Articulo[5]; //Por defecto 5
    }
    
    public Tienda(int n){
        nombre = "Informatica Cordoba";
        articulos = new Articulo[n];
    }
    
    public Tienda(String nom, int n){
        nombre = nom;
        articulos = new Articulo[n];
    }
    
    //METODOS
    public int getCantidadArticulos(){
        return articulos.length;
    }
    
    public Articulo getArticulo(int i){
        return articulos[i];
    }
    
    public void setArticulo(Articulo art, int i){
        articulos[i] = art;
    }
    
    public String buscar(int id){
        //Realiza la logica para buscar un articulo 
        //por id y que devuelva solo su nombre        
        String nombreArticulo = "No se ha encontrado el articulo";
        
        for (int i = 0; i < getCantidadArticulos(); i++) {
            Articulo auxArt = getArticulo(i);
            if(auxArt.getId() == id){
                nombreArticulo = auxArt.getNombre();
            }
        }
                
        return nombreArticulo;
    }

    @Override
    public String toString() {
        
        String cadena = "";
        
        cadena += "NOMBRE DE LA TIENDA: " + nombre;
        
        cadena += "\n=====LISTADO DE ARTICULOS=====\n";
        for(Articulo art : articulos){
            cadena += art + "\n";
        }        
        cadena += "=========================";
        
        return cadena;        
    }
    
    
    
}
