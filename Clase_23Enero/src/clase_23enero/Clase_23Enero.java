
package clase_23enero;

public class Clase_23Enero {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        int sueldo = 19000;
        
        //Si el sueldo es mayor a 3000 y es menor e igual a 9000
        // o si el sueldo es mayor a 20000 muestre que debe abonar impuesto
        // Pero si mi sueldo es 170000 o 19000 muestre que abona el doble de impuesto
        // y para el resto de valores, muestre que no debe abonar nada
        boolean primerRango = sueldo > 3000 && sueldo <= 9000;
        
        if( primerRango || sueldo > 20000){            
            System.out.println("Debe abonar impuestos.");
        }else if( sueldo == 17000 || sueldo == 19000 ){
            System.out.println("Debe abonar el doble");                        
        }else{
            System.out.println("No debe abonar impuestos");
        }
        
        System.out.println("Mi sueldo: " + sueldo);
        
        
    } //Llave del main
    
} //Llava de la clase
