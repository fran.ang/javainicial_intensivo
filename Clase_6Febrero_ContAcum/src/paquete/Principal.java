
package paquete;
public class Principal {

    public static void main(String[] args) {
        
        int contador = 0;
        System.out.println("contador: " + contador);
        
        contador++; //contador = contador + 1;
        System.out.println("contador: " + contador);
        
        contador = contador + 1; //contador++;
        System.out.println("contador: " + contador);
        
        int count = 0;
        //int postOrden = count++;
        int postOrden = count;
        count = count + 1;
        System.out.println("count: " + count);
        System.out.println("postOrden: " + postOrden);
        
        int otroCount = 0;
        //int preOrden = ++otroCount;
        otroCount = otroCount + 1;
        int preOrden = otroCount;
        System.out.println("otroCount: " + otroCount);
        System.out.println("preOrden: " + preOrden);
        
        
        int nota = 10;
        int acumulador = 1;
        acumulador = acumulador * nota; //acumulador *= nota;
        System.out.println("Acumulador: " + acumulador);
        
        nota = 8;
        acumulador = acumulador - nota; // acumulador -= nota;
        System.out.println("Acumulador: " + acumulador);
        
        nota = 2;
        acumulador /= nota; //acumulador = acumulador * nota;
        System.out.println("Acumulador: " + acumulador);
        
    }
    
}
