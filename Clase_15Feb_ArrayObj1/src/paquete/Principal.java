package paquete;

public class Principal {
    
    private static Estudiante[] estudiantes;

    public static void main(String[] args) {
        
        System.out.print("Ingrese la cantidad de estudiantes a cargar: ");
        int cant = Consola.readInt();
        
        estudiantes = new Estudiante[cant];
        
        cargar();
        
        mostrar();
                        
    } //Fin metodo main()
    
    public static void cargar(){        
        //Recorrer el arreglo - for clasico
        //Cada vuelta solicitar el id (Consola.readInt()) y el nombre (Consola.readLine())
        //En una posicion i del arreglo, instanciar (new) el objeto con el 2do constructor.
        
        for(int i = 0; i < estudiantes.length; i++){
            System.out.println("Ingrese el ID del estudiante: ");
            int id = Consola.readInt();
            System.out.println("Ingrese el nombre del estudiante: ");
            String nombre = Consola.readLine();
            
            estudiantes[i] = new Estudiante(id, nombre);
        }        
    }
    
    public static void mostrar(){
        System.out.println("== ARREGLO ESTUDIANTE ==");
        //For-loop
        for(Estudiante est : estudiantes){
            System.out.println(est);
        }        
    }
    
}
