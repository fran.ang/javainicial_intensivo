package paquete;


public class Estudiante {
    
    //ATRIBUTOS GLOBALES O DE CLASE
    private int id;
    private String nombre;
    
    //CONSTRUCTORES
    public Estudiante() {
        id = -1;
        nombre = "SIN_NOMBRE";
    }

    public Estudiante(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    
    //METODOS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();        
        sb.append("ID=").append(id);
        sb.append(", nombre=").append(nombre);        
        return sb.toString();
    }
     
}