package clase_25enero_ejercicio1;

public class Clase_25Enero_Ejercicio1 {

    public static void main(String[] args) {
        
        int numero = 337;
        String mensaje;
        
        if(numero % 2 == 0){
            mensaje = "El numero es par";
        }else{
            mensaje = "El numero es impar";
        }
        
        System.out.println(mensaje);
                
    }
    
}
