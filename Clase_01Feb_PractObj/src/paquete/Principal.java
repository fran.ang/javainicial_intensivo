
package paquete;

public class Principal {

    
    public static void main(String[] args) {
        
        //Creamos el obj auriculares
        //Sintaxis: Clase nombreObj = new Clase();
        Articulo auriculares = new Articulo(1, "Auriculares", 12500, false);
        
        System.out.println("mis auriculares: " + auriculares.toString());
        //System.out.println("El precio final de los auriculares es: " + auriculares.calcularPrecioFinal());               
        
        
        //Creamos el obj ventilador
        Articulo ventilador = new Articulo(2, "Ventilador", 14000, true);
        
        
        System.out.println("mi ventilador: " + ventilador);
        //System.out.println("El precio final del ventilador es: " + ventilador.calcularPrecioFinal());
        
        //Creamos el obj balanza
        Articulo balanza = new Articulo();
        balanza = ventilador;
        
        System.out.println("mi balanza: " + balanza);
                
        ventilador.setPrecio(15000);
        System.out.println("\nDespues de settear ventilador con 15000");
        System.out.println("Nuevo ventilador: " + ventilador);
        System.out.println("balanza: " + balanza);
        
        balanza.setNombre("Balanza");
        System.out.println("\nDespues de settear balanza con el nom Balanza");
        System.out.println("ventilador: " + ventilador);
        System.out.println("Nueva balanza: " + balanza);
        
        System.out.println("\nObtener valores especificos: ");
        System.out.println("Nombre de auriculares: " + auriculares.getNombre());
        System.out.println("Precio de auricualres: " + auriculares.getPrecio());
        
        float nvoPrecio = auriculares.getPrecio() + 500;
        auriculares.setPrecio(nvoPrecio);
        System.out.println("Nuevo precio por inflacion: " + auriculares.getPrecio());
        
        
    } //Fin del main
    
} //Fin de la clase
