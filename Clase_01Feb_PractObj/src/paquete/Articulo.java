package paquete;

public class Articulo {
    
    //ATRIBUTOS GLOBALES O DE CLASE
    private int idArticulo;
    private String nombre;
    private float precio;
    private boolean estaVendido;
    
    //CONSTRUCTORES
    //Por defecto
    public Articulo(){
        idArticulo = -1;
        nombre = "SIN_NOMBRE";
        precio = -1;
        estaVendido = false;
    }
    
    //Con parametros
    public Articulo(int idArt, String nom, float pre, boolean esVen){
        idArticulo = idArt;
        nombre = nom;
        precio = pre;
        estaVendido = esVen;
    }
    
    
    //METODOS
    public void setIdArticulo(int nvoId){
        idArticulo = nvoId;
    }
    
    public void setNombre(String nvoNombre){
        nombre = nvoNombre;
    }
    
    public void setPrecio(float nvoPrec){
        precio = nvoPrec;
    }
    
    public void setEstaVendido(boolean nvoEsVend){
        estaVendido = nvoEsVend;
    }
    
    public int getIdArticulo(){
        return idArticulo;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public float getPrecio(){
        return precio;
    }
    
    public boolean getEstaVendido(){
        return estaVendido;
    }
    
    
    public float calcularPrecioFinal(){
        //El precio final es el precio + IVA (21% del precio)
        //Pista: Valor IVA = precio * 0.21
        float precioFinal = 0;
        float impuesto = precio * 0.21f;
        precioFinal = precio + impuesto;       
        
        return precioFinal;
    }
    
    @Override
    public String toString(){
        //Retorna los valores de los atributos de la clase
        return "ID Articulo: " + idArticulo 
                + ", Nombre: " + nombre
                + ", Precio: " + precio + "$"
                + ", ¿esta vendido? : " + estaVendido;
    }
    
}
