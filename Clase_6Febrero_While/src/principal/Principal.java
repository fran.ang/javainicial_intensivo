
package principal;

public class Principal {

    public static void main(String[] args) {
        
        int nro;
        int acum = 0;
        int count = 0;
        
        System.out.println("Ingrese un numero: ");
        nro = Consola.readInt();
        
        while( nro != 0 ){    
            count++; //Contar un nro nuevo
            acum += nro; //Acumula los valores de nro.
            System.out.println("Acumulador: " + acum); 
            System.out.println("Contador: " + count);
            
            System.out.println("Ingresa otro numero: ");
            nro = Consola.readInt();
        }
        
        if(acum != 0 && count != 0){
            int promedio = acum / count;
            System.out.println("Promedio: " + promedio);
        } else {
            //Se rompe si la division es 0/0
            System.out.println("Debería cargar un valor");
        }
        
        
    }
    
}
