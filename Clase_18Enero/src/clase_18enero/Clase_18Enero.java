package clase_18enero;

public class Clase_18Enero {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //System
        //out: salida
        //print de imprimir
        //ln line
        System.out.println("Hola mundo!");

        //la primera letra de los comando y tab(sout)+tab
        System.out.println("Otro hola mundo");

        int edad = 50;
        System.out.println(edad);

        char letra = 'j';
        System.out.println("Variable llamada letra: " + letra);

        int alturaPersona = 160;
        String cadenaAltPer = alturaPersona + " cm";
        System.out.println("La altura de la persona es: " + cadenaAltPer + ", y su edad es de: " + edad);

        // ======= ACTIVIDADES =======
        //ENTRADAS
        float lado1 = 30.4f;
        float lado2 = 40.9f;
        float lado3 = 50.879f;

        //PROCESO
        float perimetro = lado1 + lado2 + lado3;

        //SALIDA
        System.out.println("Perimetro = " + perimetro + " cm");

        //ENTRADA
        int cantRtaCorrectas = 5;
        int cantRtaIncorrectas = 3;

        //PROCESO
        int ptjeFinal = cantRtaCorrectas * 4 - cantRtaIncorrectas;

        //SALIDA
        System.out.println("Cant. Rtas Correctas: " + cantRtaCorrectas);
        System.out.println("Cant. Rtas Incorrectas: " + cantRtaIncorrectas);
        System.out.println("Puntaje final: " + ptjeFinal + " puntos");

        //ENTRADAS
        int nro1 = 250;
        int nro2 = 500;
        int mayor;
        boolean esMayor = nro1 > nro2;

        //PROCESO
        if (esMayor) {
            //Bloque verdadero del if
            mayor = nro1;
            System.out.println("El mayor es: " + mayor);
        } 
        
        System.out.println("Cant. Rtas Correctas: " + cantRtaCorrectas);
        System.out.println("Cant. Rtas Incorrectas: " + cantRtaIncorrectas);
        System.out.println("Puntaje final: " + ptjeFinal + " puntos");

        //SALIDA
    } //Termina el main

} //Termina la clase

