
package clase_25enero_ejercicio2;

public class Clase_25Enero_Ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int calificacion = 80;
        String nota = "";
        
        //RANGO DE 0 A 100 INCLUIDOS
        //SI SUPERA, MOSTRAR QUE EL RANGO NO ES VALIDO
        if(calificacion < 0 || calificacion > 100){
            nota = "La calificacion esta fuera del rango";
        }else if(calificacion > 90){
            nota = "A";
        }else if(calificacion > 80){
            nota = "B";
        }else if(calificacion > 70){
            nota = "C";
        }else if(calificacion > 60){
            nota = "D";
        }else{
            nota = "F";
        }
        
        System.out.println("Mi nota es: " + nota);
        
    }
    
}
