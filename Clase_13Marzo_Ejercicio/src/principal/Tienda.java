package principal;

public class Tienda {
    //ATRIBUTOS GLOBALES
    private String nombre;
    private Mouse[] mouses;
    private Teclado[] teclados;
    
    //CONSTRUCTORES
    public Tienda(){
        nombre = "Informatica Cordoba";
        
        //instanciar arreglo mouses
        mouses = new Mouse[2];
        //instanciar arreglo teclados
        teclados = new Teclado[2];
    }
    
    /**
     * Crea un arreglo de componentes de mouse de n elementos, y uno de 
     * teclados de m elementos.      
     * @param n el tamanio del arreglo de mouse.
     * @param m el tamanio del arreglo de teclados.
     */
    public Tienda(int n, int m){
        nombre = "Informatica Cordoba";        
        mouses = new Mouse[n];
        //instanciar arreglo teclados
        teclados = new Teclado[m];
        
    }
        
    public int getCantidadMouses()
    {
        return mouses.length;   
    }
    
    public int getCantidadTeclados()
    {
        return teclados.length; 
    }

    
    public void setMouse(Mouse mou, int i)
    {
        mouses[i] = mou;
    }
    
    public void setTeclado(Teclado tec, int i)
    {
        teclados[i] = tec;
    }
    
    //METODOS PARA IMPLEMENTAR EL MENU DE OPCIONES
    public void actualizarPrecios(){
        //Recorrer mouses
        for (Mouse mouse : mouses) {
            System.out.println("\n" + mouse);
            System.out.println("\tAnterior precio: " + mouse.getPrecio());
            mouse.actualizarPrecio();
            System.out.println("\tNuevo precio: " + mouse.getPrecio());
        }
        
        //Recorrer teclados
        for (Teclado teclado : teclados) {
            System.out.println("\n" + teclado);
            System.out.println("\tAnterior precio: " + teclado.getPrecio());
            teclado.actualizarPrecio();
            System.out.println("\tNuevo precio: " + teclado.getPrecio());
        }
    }
    
    public String filtrarPorMarca(String marca){
        String cadena = "COMPONENTES FILTRADOS POR: " + marca + "\n";
        int contador = 0;
        
        //Recorrer mouses
        for (Mouse mouse : mouses) {
            if(mouse.getMarca().equalsIgnoreCase(marca)){
                cadena += mouse + "\n";
                contador++;
            }
        }
        
        //Recorrer teclados
        for (Teclado teclado : teclados) {
            if(teclado.getMarca().equalsIgnoreCase(marca)){
                cadena += teclado + "\n";
                contador++;
            }
        }
        
        if(contador == 0){
            cadena += "La marca no existe en la tienda";
        }
        
        return cadena;
    }
    
    public String filtrarPorCantTeclas(int cantTeclas){
        String cadena = "===COMPONENTES FILTRADOS POR CANT. DE TECLAS MAYOR A : " + cantTeclas + "===\n";
        
        for (Teclado teclado : teclados) {
            if(teclado.getCantidadTeclas() > cantTeclas){
                cadena += teclado + "\n\t";
            }
        }
        
        return cadena;
    }
    
    public String cambiarMarca(int codBarra, String nuevaMarca){
        String rpta = "No se ha encontrado el mouse";
        
        for (int i = 0; i < getCantidadMouses(); i++) {
            Mouse mouseAux = mouses[i];
            if(mouseAux.getCodigoDeBarra() == codBarra){
                mouseAux.setMarca(nuevaMarca);
                rpta = "Se encontro el mouse y su marca se modifico con exito.";
                break;                
            }
        }        
        return rpta;
    }
    
    public String cambiarCantTeclas(int codBarra,int nvaCantTeclas){
        String rpta = "No se ha encontrado el teclado";
        
        for (Teclado teclado : teclados) {
            if(teclado.getCodigoDeBarra() == codBarra){
                teclado.setCantidadTeclas(nvaCantTeclas);
                rpta = "Se encontro el teclado y su cantidad de teclas se modifico con exito.";
                break;                
            }
        }        
        return rpta;
    }
    
    @Override
    public String toString(){
        String cadena = "Tienda: " + nombre + "\nMouse disponibles:\n\t";
        
        //Recorrer arreglo mouses
        for (Mouse mouse : mouses) {
            cadena += mouse + "\n\t";
        }
        
        cadena += "\nTeclados disponibles:\n\t";
        
        //Recorrer arreglo teclados
        for (Teclado teclado : teclados) {
            cadena += teclado + "\n\t";
        }
        
        return cadena;
    }
       
    
}//Fin de la clase
