package principal;

public class Teclado extends Componente{
    
    //ATRIBUTO DE LA CLASE
    private int cantidadTeclas;
    
    //CONSTRUCTORES

    public Teclado() {
        //super();
    }

    public Teclado(int cantidadTeclas, int codigoDeBarra, String marca, 
            String pais, float precio) {
        super(codigoDeBarra, marca, pais, precio);
        this.cantidadTeclas = cantidadTeclas;
    }
    
    //METODOS

    public int getCantidadTeclas() {
        return cantidadTeclas;
    }

    public void setCantidadTeclas(int cantidadTeclas) {
        this.cantidadTeclas = cantidadTeclas;
    }

    @Override
    public String toString() {
        return super.toString() + " - Es TECLADO{" + "Cantidad de teclas: " 
                + cantidadTeclas + '}';
    }
    
}
