package principal;

public class Componente {
    //ATRIBUTOS GLOBALES
    private int codigoDeBarra;
    private String marca;
    private String pais;
    private float precio;

    //CONSTRUCTORES
    public Componente() {
    }

    public Componente(int codigoDeBarra, String marca, String pais, float precio) {
        this.codigoDeBarra = codigoDeBarra;
        this.marca = marca;
        this.pais = pais;
        this.precio = precio;
    }
        
    
    //METODOS
    public int getCodigoDeBarra() {
        return this.codigoDeBarra;
    }

    public void setCodigoDeBarra(int codigoDeBarra) {
        this.codigoDeBarra = codigoDeBarra;
    }

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public void actualizarPrecio(){
        float porcentaje;
        
        if(pais.equalsIgnoreCase("Argentina")){
            porcentaje = 0.05f;
        }else if(pais.equalsIgnoreCase("EEUU")){
            porcentaje = 0.1f;
        }else if(pais.equalsIgnoreCase("Alemania")){
            porcentaje = 0.15f;
        }else{
            porcentaje = 0.3f;
        }
        
        float incremento = precio * porcentaje;
        precio += incremento;        
    }


    @Override
    public String toString() {
        return "Componente { Cod. de barra: " + codigoDeBarra
                + ", marca: " + marca
                + ", pais de fab.: " + pais
                + ", precio: " + precio + "$ }";
    }
    
}
