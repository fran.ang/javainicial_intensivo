package principal;

public class Mouse extends Componente{
    //ATRIBUTOS GLOBALES O DE CLASE
    private boolean esInalambrico;    

    //CONSTRUCTORES

    public Mouse(boolean esInalambrico) {
        this.esInalambrico = esInalambrico;
    }

    public Mouse(boolean esInalambrico, int codigoDeBarra, String marca, String pais, float precio) {
        super(codigoDeBarra, marca, pais, precio);
        this.esInalambrico = esInalambrico;
    }
    

    //METODOS
    public boolean getEsInalambrico() {
        return esInalambrico;
    }

    public void setEsInalambrico(boolean esInalambrico) {
        this.esInalambrico = esInalambrico;
    }
    
   
    @Override
    public String toString() {
        return super.toString() + " - Es MOUSE { Inalambrico: " + esInalambrico + "}";
    }
    
}
