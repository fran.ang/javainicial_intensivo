package principal;

import javax.swing.JOptionPane;

public class Principal {
    
    private static Tienda tien; //Solo se declara
    
    public static void main(String[] args) {        
        
        int op;
        do
        {
            System.out.println("1. Inicializar / Reiniciar componentes de mouse y teclado.");                       
            System.out.println("2. Cargar mouse");
            System.out.println("3. Cargar teclados");
            System.out.println("4. Mostrar ambos tipos de componentes");
            System.out.println("5. Actualizar precios");
            System.out.println("6. Filtrar componentes según la marca");
            System.out.println("7. Filtrar teclados según la cantidad de teclas solicitada");
            System.out.println("8. Buscar un mouse y cambiar su marca");
            System.out.println("9. Buscar un teclado y cambiar la cant. de teclas");
            System.out.println("10. Salir");
            System.out.print("\t\tIngrese: ");
            op = Consola.readInt();
            
            switch(op)
            {
                case 1:
                        String cantMouseStr = JOptionPane.showInputDialog(null, "Ingresar la cantidad de mouse: ");
                        int cantMouse = Integer.parseInt(cantMouseStr);
                        String cantTeclStr = JOptionPane.showInputDialog(null, "Ingresar la cantidad de teclados: ");
                        int cantTecl = Integer.parseInt(cantTeclStr);
                        tien = new Tienda(cantMouse, cantTecl);
                        break;                      
                case 2:
                        cargarMouses();
                        break;                        
                case 3:
                        cargarTeclados();
                        break;                        
                case 4:
                        System.out.println(tien);
                        break;               
                case 5:   
                        tien.actualizarPrecios();
                        break;                          
                case 6: String marca = JOptionPane.showInputDialog(null, "Ingrese una marca para filtrar:");
                        String filtrado = tien.filtrarPorMarca(marca);
                        System.out.println(filtrado);
                        break;                          
                case 7: String cantTeclasStr = JOptionPane.showInputDialog(null, "Ingrese la cant. teclas para filtrar:");
                        int cantTeclas = Integer.parseInt(cantTeclasStr);
                        String filtradoPorTeclas = tien.filtrarPorCantTeclas(cantTeclas);
                        System.out.println(filtradoPorTeclas);
                        break;   
                       
                case 8: String cBarraStr = JOptionPane.showInputDialog(null, "Ingrese el codigo barra:");
                        int cBarra = Integer.parseInt(cBarraStr);
                        String nvaMarca = JOptionPane.showInputDialog(null, "Ingrese la nueva marca:");                                                                        
                        String cambiarMarca = tien.cambiarMarca(cBarra, nvaMarca);
                        JOptionPane.showMessageDialog(null, cambiarMarca, "Resultado", JOptionPane.INFORMATION_MESSAGE);
                        break;   
                        
                case 9: String codBarraStr = JOptionPane.showInputDialog(null, "Ingrese el codigo barra:");
                        int codBarra = Integer.parseInt(codBarraStr);
                        String nvaCantTecStr = JOptionPane.showInputDialog(null, "Ingrese la nueva cantidad de teclas:");                                                                        
                        int nvaCantTec = Integer.parseInt(nvaCantTecStr);
                        String cambiarCantTeclas = tien.cambiarCantTeclas(codBarra, nvaCantTec);
                        JOptionPane.showMessageDialog(null, cambiarCantTeclas, "Resultado", JOptionPane.INFORMATION_MESSAGE);
                        break;   
                       
                case 10: System.out.println("GRACIAS POR UTILIZAR EL PROGRAMA");
                        break;
                default: System.out.println("Opcion incorrecta. Por favor, "
                        + "ingrese un valor entre 0 y 10.");
            }
        }
        while( op != 10 );
        
        
    } //Fin del metodo main
    
    /**
     * Carga por teclado los datos de los mouse. Por consola.
     */
    public static void cargarMouses()
    {
        for(int i = 0; i < tien.getCantidadMouses(); i++)
        {
            System.out.println("== CARGA DEL MOUSE NRO: " + i + " ==");
            System.out.print("Codigo de barra: ");
            int codBarra = Consola.readInt();
            
            System.out.print("Marca: ");
            String marca = Consola.readLine();
            
            System.out.print("Pais de fabricacion: ");
            String pais = Consola.readLine();
         
            System.out.print("Precio: ");
            float pre = (float)Consola.readDouble();
            
            System.out.println("Es inalambrico?");
            String rta = Consola.readLine();
            boolean esIna = rta.equalsIgnoreCase("SI");           
            
            Mouse mouse = new Mouse(esIna, codBarra, marca, pais, pre);
            tien.setMouse(mouse, i);
        }   
    }
    
    /**
     * Carga por teclado los datos de los teclados. Usando cuadro de dialogos.
     */
    public static void cargarTeclados() 
    {
        for(int i = 0; i < tien.getCantidadTeclados(); i++)
        {
            String mensaje = "== CARGA DEL TECLADO NRO: " + i + " ==";
            String titulo = "TECLADO NRO " + i;
            JOptionPane.showMessageDialog(null, mensaje, "Carga", JOptionPane.WARNING_MESSAGE);
            String codBarraStr = JOptionPane.showInputDialog(null, "Ingrese el codigo de barra", titulo, JOptionPane.QUESTION_MESSAGE);
            int codBarra = Integer.parseInt(codBarraStr);
            
            String marca = JOptionPane.showInputDialog(null, "Ingrese la marca: ", titulo, JOptionPane.QUESTION_MESSAGE);                                                            
            
            String pais = JOptionPane.showInputDialog(null, "Pais de fabricacion: ", titulo, JOptionPane.QUESTION_MESSAGE);                        
         
            String precioStr = JOptionPane.showInputDialog(null, "Ingrese el precio: ", titulo, JOptionPane.QUESTION_MESSAGE);  
            float pre = Float.parseFloat(precioStr);            
            
            String cantTecStr = JOptionPane.showInputDialog(null, "Cantidad de teclas: ", titulo, JOptionPane.QUESTION_MESSAGE);              
            int cantTec = Integer.parseInt(cantTecStr);
            
            Teclado teclado = new Teclado(cantTec, codBarra, marca, pais, pre);
            tien.setTeclado(teclado, i);
        }      
    }
    

} //Fin de la clase