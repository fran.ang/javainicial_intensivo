package paquete;

public class Principal {
    
    private static Curso curso;

    public static void main(String[] args) {
        
        System.out.print("Ingrese la cantidad de estudiantes a cargar: ");
        int cant = Consola.readInt();
        
        curso = new Curso(cant);
        
        cargar();
        
        System.out.println(curso);
        
        System.out.print("\nIngresar el promedio para participar en el curso: ");
        float prom = (float) Consola.readDouble();
        mostrarAceptados(prom);
        
        
    } //Fin metodo main()
    
    public static void cargar(){                        
        for(int i = 0; i < curso.getCantidadInscriptos(); i++){
            System.out.println("Ingrese el ID del estudiante: ");
            int id = Consola.readInt();
            System.out.println("Ingrese el nombre del estudiante: ");
            String nombre = Consola.readLine();
            System.out.println("Ingrese el promedio del estudiante: ");
            float prom = (float) Consola.readDouble();
            
            Estudiante est = new Estudiante(id, nombre, prom);
            curso.setEstudiante(est, i);
        }    
    
    } //Fin del cargar()   
    
    public static void mostrarAceptados(float x){
        System.out.println("\n== LISTA DE ACEPTADOS ==");
        
        //Antes de que filtres, ordenate
        curso.ordenar();
        
        //Filtro por nota
        for(int i = 0; i < curso.getCantidadInscriptos(); i++){
            Estudiante est = curso.getEstudiante(i);
            if(est.getPromedio() > x){
                System.out.println(est);
            }
        }
    }
    
}
