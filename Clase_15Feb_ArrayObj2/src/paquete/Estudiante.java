package paquete;


public class Estudiante {
    
    //ATRIBUTOS GLOBALES O DE CLASE
    private int id;
    private String nombre;
    private float promedio;
    
    //CONSTRUCTORES
    public Estudiante() {
        id = -1;
        nombre = "SIN_NOMBRE";
        promedio = -1;
    }

    public Estudiante(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        promedio = -1; //Por defecto
    }

    public Estudiante(int id, String nombre, float promedio) {
        this.id = id;
        this.nombre = nombre;
        this.promedio = promedio;
    }
        
    
    //METODOS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPromedio() {
        return promedio;
    }

    public void setPromedio(float promedio) {
        this.promedio = promedio;
    }
        

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();        
        sb.append("ID=").append(id);
        sb.append(", nombre=").append(nombre);
        sb.append(", promedio=").append(promedio);
        return sb.toString();
    }
     
}