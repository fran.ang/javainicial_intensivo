
package paquete;

public class Curso {
    
    //ATRIBUTOS GLOBALES O DE CLASE
    private String titulo;
    private Estudiante[] inscriptos;
    
    //CONSTRUCTOR
    public Curso() {
        titulo = "Clase Programacion Java";
        inscriptos = new Estudiante[5];
    }
    
    public Curso(int tamanio) {
        titulo = "Clase Programacion Java";
        inscriptos = new Estudiante[tamanio];
    }
        
    public Curso(int tamanio, String tit) {
        titulo = tit;
        inscriptos = new Estudiante[tamanio];
    }
    
    //METODOS
    public int getCantidadInscriptos(){
        return inscriptos.length;
    }
    
    public Estudiante getEstudiante(int i){
        return inscriptos[i];
    }
    
    public void setEstudiante(Estudiante est, int i){
        inscriptos[i] = est;
    }
    
    public void ordenar() {
        for (int i = 0; i < inscriptos.length - 1; i++) {
            for (int j = 0; j < inscriptos.length - i - 1; j++) {
                if (inscriptos[j + 1].getId() < inscriptos[j].getId()) {
                    Estudiante aux = inscriptos[j + 1];
                    inscriptos[j + 1] = inscriptos[j];
                    inscriptos[j] = aux;
                }
            }
        }
    }

    @Override
    public String toString(){
        String cadena = "";
        
        cadena += "TITULO: " + titulo;
        cadena += "\n== LISTA DE ESTUDIANTES ==\n"; // \n: salto de linea
        for(Estudiante est : inscriptos){
            cadena += est + "\n";
        }               
        
        return cadena;
    }   
}
